package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func changecolor(c *gin.Context) {
	word := c.Params.ByName("color")
	fmt.Println(word)
	c.JSON(200, gin.H{"color": word})

}

func main() {
	router := gin.Default()
	router.LoadHTMLGlob("templates/*")

	router.GET("/index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"title": "Main website",
		})
	})
	router.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})
	router.GET("/changetocolor/:color", changecolor)

	router.Run(":8080")
}
